const app = require('./app');
const https = require('https');
const fs = require('fs');
const port = 8084;

app.listen(port,console.log(`The server is listening on port ${port}`));

const  options = {
    //     requestCert: false,
           key: fs.readFileSync(("/home/ec2-user/SSL/cadence-commerce.com.key"), 'utf8'),
           cert: fs.readFileSync(("/home/ec2-user/SSL/ssl-bundle.crt"), 'utf8')
           //ca: fs.readFileSync("/home/ec2-user/SSL/cadence-commerce.com.ca")
           //ca: fs.readFileSync(("/home/ec2-user/SSL/cadence-commerce.com.ca-bundle"), 'utf8')
    // ca: [
     //      fs.readFileSync('path/to/COMODORSAXXXXXXXXXSecureServerCA.crt'),
     //      fs.readFileSync('path/to/COMODORSAAddTrustCA.crt')
     // ]
    
    };
    
    
    
    https.createServer(options, function (req, res) {
    
    app.handle(req,res);
    
    }).listen(7084);