const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const app = express();
const router = express.Router();
const cors=require('cors');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
//Middleware
app.use('/', router);
router.use((req, res, next)=> {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.locals.currentUser = req.user;
    next();
});


//Route variables
//const index = require('./routes/index');
const products= require('./routes/product/api');

//Routing
//app.use('/',index);

app.use('/api/products',products);
app.get("/getallproducts",function(req,res){

    res.render("getallproducts");
 
});
app.get("/getproduct",function(req,res){
    
        res.render("getproduct");
     
    });
    app.get("/deleteproduct",function(req,res){
        
            res.render("deleteproduct");
         
        });
        app.get("/addproduct",function(req,res){
            
                res.render("addproduct");
             
            });
            app.get("/updateproduct",function(req,res){
                
                    res.render("updateproduct");
                 
                });

app.get('*', function(req, res){
    // res.send('OOPS! something went wrong', 404);
    res.render('err');
  });

  app.get('/api/product/*', function(req, res){
    // res.send('OOPS! something went wrong', 404);
    res.render('err');
  });

module.exports = app;
