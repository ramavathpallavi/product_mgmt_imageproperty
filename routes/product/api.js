const router = require('express').Router();
const Product = require('../../models/product');
const Methods = require('../../methods/custom');
const statusMsg = require('../../methods/statusMsg');
const jwt = require('jsonwebtoken');
const config = require('../../config/config');

var multer = require('multer');
var aws = require('aws-sdk');
var fs = require('fs');
var s3 = new aws.S3();

/* Multer set storage location*/
var storage = multer.diskStorage({

	filename: function (req, file, cb) {
		var str = file.originalname;
		str = str.replace(/\s+/g, '-').toLowerCase();
		global.poster = Date.now() + '_' + str;
		cb(null, poster);
	}
});

var upload = multer({ storage: storage });
aws.config.update({ accessKeyId: config.ACCESS_KEY_ID, secretAccessKey: config.SECRET_ACCESS_KEY });
aws.config.update({ region: config.REGION });

//Image delete from S3 bucket
var s3client = new aws.S3({
	accessKeyId: config.ACCESS_KEY_ID, secretAccessKey: config.SECRET_ACCESS_KEY, params: {
		Bucket: config.BUCKET_NAME,
	},
});


router.get('/', (req, res) => {
	res.send('This is the api route for Product management');
});

router.get('/getallproducts', (req, res) => {
	var token = req.body.token || req.headers['token'];

	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				const table = `${decode.storeName}_store_data`;

				Product.selectTable(table);
				Product.query('product')
				.then((products) => {
					res.send(products);
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})
			}
		})
	}
	else {
		res.send("please send a token");
	}
});

router.post('/getproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];

	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				const tabletemp = `${decode.storeName}_store_data`;

				Product.selectTable(tabletemp);
				const bodyParams = Methods.initializer(req, Product);

				Product.getItem(bodyParams, {})
				.then((product) => {
					res.json(product);
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})

			}
		})
	}
	else {
		res.send("please send a token");
	}
});

router.get('/editproduct/:collectionType/:collectionId', (req, res) => {
	const token = req.body.token || req.headers['token'];
	const params = req.params;

		if(token){
			jwt.verify(token, config.SECRET_KEY, function(err, decode){
				if(err){
					res.status(500).send("Invalid Token");
				}
				else{
					const table = `${decode.storeName}_store_data`;

                    Product.selectTable(table);
					Product.getItem(params, {})
					.then((product) => {
						res.json(product);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})
				}
			})
		}
		else{
			res.send("please send a token");
		}	
});

router.post('/add', upload.single('image'), (req, res) => {
	var token = req.body.token || req.headers['token'];
	var hello = req.file.path;
	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				s3.upload({
					"ACL": "public-read",
					"Bucket": config.BUCKET_NAME,
					"Key": poster,
					"Body": fs.createReadStream(hello),
					ContentType: "image/jpeg"
				}, function (err, data) {
					if (err) {
						console.log("Error uploading data: ", err);
					} else {
						console.log("\nImage upload success\n");
						const imageurl = "https://s3.ap-south-1.amazonaws.com/"+config.BUCKET_NAME+"/" + poster;

						const bodyParams = Methods.initializer(req, Product);
						bodyParams['imageURL'] = imageurl;
						
						Product.createItem(bodyParams, {
							table: decode.storeName + "_store_data",
							overwrite: false
						}).then((product) =>{
							res.send(product);
						}).catch((err) => {
							res.send(statusMsg.errorResponse(err));
						})
					}
				});

			}

		});

	}else {
		res.send("please send a token");
	}
});

router.post('/addproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
						// console.log("\nImage upload success\n");
						// const imageurl = "https://s3.ap-south-1.amazonaws.com/"+config.BUCKET_NAME+"/" + poster;

						const bodyParams = Methods.initializer(req, Product);
						//bodyParams['imageURL'] = imageurl;
						
						Product.createItem(bodyParams, {
							table: decode.storeName + "_store_data",
							overwrite: false
						}).then((product) =>{
							res.send(product);
						}).catch((err) => {
							res.send(statusMsg.errorResponse(err));
						})

			}

		});

	}else {
		res.send("please send a token");
	}
});


router.put('/updateproduct', upload.single('image') ,(req, res) => {
	var token = req.body.token || req.headers['token'];
	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {
				if(req.file == undefined){
					//console.log(req.body);
					var updateParams = {
						collectionType:req.body.collectionType,
						collectionId: req.body.collectionId,
						title:req.body.title,
						description:req.body.description,
						price: req.body.price
					};
					//console.log("product:::"+JSON.stringify(updateParams));
					const table = `${decode.storeName}_store_data`;
					Product.selectTable(table);
					Product.updateItem(updateParams, {})
					.then((product) =>{
						res.send(product);
					}).catch((err) => {
						res.send(statusMsg.errorResponse(err));
					})

				}else{
					//console.log("req value: "+JSON.stringify(req.file));
					var s3 = new aws.S3();
					var hello=req.file.path;
					s3.upload({
						"ACL": "public-read",
						"Bucket": config.BUCKET_NAME,
						"Key": poster,
						"Body": fs.createReadStream(hello),
						ContentType: "image/jpeg"
								
						}, function(err, data) {
						if (data) {
							console.log("\nImage upload success");
							const imageurl="https://s3.ap-south-1.amazonaws.com/"+config.BUCKET_NAME+"/"+poster;
							var updateParams = {
								collectionType:req.body.collectionType,
								collectionId: req.body.collectionId,
								title:req.body.title,
								description:req.body.description,
								price: req.body.price,
								imageURL:imageurl
							};

							//console.log("Product1:::"+JSON.stringify(updateParams));
							const table = `${decode.storeName}_store_data`;
							Product.selectTable(table);
							Product.updateItem(updateParams, {})
							.then((product) =>{
									const url=req.body.preImage;
									const Imagename=url.substring(url.lastIndexOf('/')+1);

									s3client.deleteObject({
										Key: Imagename,
									}, function (err, data) {
										if (err) {
											console.log("Error deleting data: ", err);
										}
										else {
											res.send("product updated successfully");
										}
				
									})
							}).catch((err) => {
								res.send(statusMsg.errorResponse(err));
							})
						}
					});
				}
			}
		})
	}
	else {
		res.send("please send a token");
	}
});

router.delete('/deleteproduct', (req, res) => {
	var token = req.body.token || req.headers['token'];
	const bodyParams = req.body;
	//console.log("params:"+bodyParams);

	if (token) {
		jwt.verify(token, config.SECRET_KEY, function (err, decode) {
			if (err) {
				res.status(500).send("Invalid Token");
			}
			else {

				const table = `${decode.storeName}_store_data`
				Product.selectTable(table);
				Product.deleteItem({collectionType: req.body.collectionType,collectionId: req.body.collectionId}, { ReturnValues: 'ALL_OLD' })
				.then((removeData) =>{
					
					var url = removeData.imageURL;
					const Imagename=url.substring(url.lastIndexOf('/')+1);

					s3client.deleteObject({
						Key: Imagename,
					}, function (err, data) {
						if (err) {
							console.log("Error deleting data: ", err);
						}
						else {	
							res.send(removeData);
						}

					})
				}).catch((err) => {
					res.send(statusMsg.errorResponse(err));
				})
			}
		})
	}
	else {
		res.send("please send a token");
	}
});

module.exports = router;
